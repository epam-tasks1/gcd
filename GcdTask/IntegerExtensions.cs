﻿using System;

namespace GcdTask
{
    public static class IntegerExtensions
    {
        public static int FindGcd(int a, int b)
        {
            if (a == 0 && b == 0)
            {
                throw new ArgumentException("All numbers are 0 at the same time.");
            }

            if (a == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a));
            }

            if (b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(b));
            }

            a = Math.Abs(a);
            b = Math.Abs(b);

            if (a == b)
            {
                return a;
            }

            if (a < b)
            {
                (a, b) = (b, a);
            }

            if (b == 0)
            {
                return a;
            }

            int remainderOfDivision;
            int divisionNumber = b;
            int dividerNumber = a;
            bool checkСondition = true;

            while (checkСondition)
            {
                remainderOfDivision = dividerNumber % divisionNumber;

                if (remainderOfDivision == 0)
                {
                    return b;
                }

                dividerNumber = (dividerNumber - remainderOfDivision) / (dividerNumber / divisionNumber);
                divisionNumber = remainderOfDivision;

                if (dividerNumber % remainderOfDivision == 0)
                {
                    checkСondition = false;
                }
            }

            return divisionNumber;
        }
    }
}
